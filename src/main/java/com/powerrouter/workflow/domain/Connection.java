/**
 * 
 */
package com.powerrouter.workflow.domain;

import java.util.List;

/**
 * @author abhinab
 *
 */
public class Connection {

  private int order;
  private List<Component> components;

  public int getOrder() {
    return order;
  }

  public void setOrder(int order) {
    this.order = order;
  }

  public List<Component> getComponents() {
    return components;
  }

  public void setComponents(List<Component> components) {
    this.components = components;
  }

}
