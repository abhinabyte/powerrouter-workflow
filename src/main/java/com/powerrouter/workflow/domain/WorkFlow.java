/**
 * 
 */
package com.powerrouter.workflow.domain;

import java.util.List;

/**
 * @author abhinab
 *
 */
public class WorkFlow {

  private String name;
  private List<Connection> connections;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public List<Connection> getConnections() {
    return connections;
  }

  public void setConnections(List<Connection> connections) {
    this.connections = connections;
  }

}
