/**
 * 
 */
package com.powerrouter.workflow.domain;

import java.util.Map;

/**
 * @author abhinab
 *
 */
public class Component {

  private String name;
  private String type;
  private int order;
  private Map<String, Object> config;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public int getOrder() {
    return order;
  }

  public void setOrder(int order) {
    this.order = order;
  }

  public Map<String, Object> getConfig() {
    return config;
  }

  public void setConfig(Map<String, Object> config) {
    this.config = config;
  }

}
