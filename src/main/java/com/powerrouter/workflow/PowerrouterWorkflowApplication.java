package com.powerrouter.workflow;

import java.io.IOException;
import java.io.InputStream;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.powerrouter.workflow.domain.Component;
import com.powerrouter.workflow.domain.Connection;
import com.powerrouter.workflow.domain.WorkFlow;

/**
 * 
 * @author abhinab
 *
 */
public class PowerrouterWorkflowApplication {

  private static final ObjectMapper mapper = new ObjectMapper();

  public static String name = "Fname Lname";

  public static void main(String[] args) throws IOException {
    try (InputStream in = PowerrouterWorkflowApplication.class.getClassLoader()
        .getResourceAsStream("workflow.json")) {
      WorkFlow workFlow = mapper.readValue(in, WorkFlow.class);

      List<Connection> connections = workFlow.getConnections();

      connections.sort(Comparator.comparing(Connection::getOrder));

      connections.forEach(connection -> {
        List<Component> components = connection.getComponents();

        components.sort(Comparator.comparing(Component::getOrder));

        components.forEach(component -> {
          String componentName = component.getName();
          String componentType = component.getType();
          Map<String, Object> componentConfig = component.getConfig();

          name = name + "do some thing based on component config";

          System.out.println(name);
        });
      });
    }
  }

}
